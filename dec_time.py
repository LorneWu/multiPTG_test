# -*- coding: utf-8 -*-
import os
def dec_time(func):
    def wrapper(*args,**kargs):
        import time
        import csv
        t_start = time.time()
        t_start_c = time.clock()
        res = func(*args,**kargs)
        t_end = time.time()
        t_end_c  = time.clock()
        print(func.__name__,kargs,'real time :',t_end-t_start)
        print(func.__name__,kargs,'wall time :',t_end_c - t_start_c)
        if not os.path.exists('result.csv'):
            with open('result.csv','w',encoding='utf-8') as csvfile:
                fieldnames = ['function', 'kargs','args','real_time','wall_time']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
        with open('result.csv','a',encoding='utf-8') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"')
            spamwriter.writerow([func.__name__,len(args[0]),str(kargs),t_end-t_start,t_end_c - t_start_c])
        return res
    return wrapper