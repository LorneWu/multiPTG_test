# -*- coding: utf-8 -*-

import multiprocessing as mp
from random import choice

lis = [i for i in range(100)]
def func(i):
    global lis
    lis.remove(i)
    print(i,len(lis))

print(lis)
p = mp.Pool(4)
for i in lis:
    p.apply(func,(i,))
#p.map_async(func,lis)
import time
time.sleep(1)
print(lis)