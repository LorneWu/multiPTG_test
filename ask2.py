# -*- coding: utf-8 -*-
import gevent
from gevent import monkey;monkey.patch_all()
import grequests
import headers
import requests
from bs4 import BeautifulSoup as bs
import time
import logging


s = requests.session()

def ask_gevent(url,sleeptime=0,timeout=0):
    try:
        if timeout==0:r = s.get(url,headers = headers.getprofile())
        else:r = s.get(url,headers = headers.getprofile(),timeout = timeout)
    except Exception as e:
        #logging.warning(e)
        with open('wrong.log','a')as f:f.write(str(e)+'\n')
    gevent.sleep(sleeptime)
'''
def ask_gevent(url,sleeptime=0,timeout=0):
    if timeout==0:r = s.get(url,headers = headers.getprofile())
    else:r = s.get(url,headers = headers.getprofile(),timeout = timeout)
'''
def exception_handler(request, exception):
    #print(exception)
    pass

def ask_grequests(urls,timeout=0):
    reqs = [grequests.get(url,timeout=timeout) for url in urls]
    s = grequests.map(reqs,exception_handler=exception_handler)
    return s