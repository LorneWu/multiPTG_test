# -*- coding: utf-8 -*-

import _thread
from ask import ask
import threading
from dec_time import dec_time
from configparser import ConfigParser;config = ConfigParser()
config.read('setting.ini')

class Thread (threading.Thread):
    def __init__(self,url,sleeptime=0,timeout=0):
        threading.Thread.__init__(self)
        self.url = url
        self.sleeptime=0
        self.timeout=0
    def run(self):
        ask(self.url,self.sleeptime,self.timeout)

@dec_time
def simpleThread(lis,sleeptime=0,timeout=0):
    #lock = _thread.allocate_lock()
    global num_thread
    num_thread = 0
    def handle(i,sleeptime=0,timeout=0):
        global num_thread
        num_thread +=1
        ask(i,sleeptime,timeout)
        num_thread -=1
        
    for i in lis:_thread.start_new_thread(handle,(i,sleeptime,timeout))
    from time import sleep
    sleep(1)
    while num_thread > 0:pass

@dec_time
def complexThread(lis,sleeptime=0,timeout=0):
    thr_list = [] 
    for i in lis:
        thr = Thread(i,sleeptime,timeout)
        thr_list.append(thr)
        thr.start()
    for j in thr_list:
        j.join()
#simpleThread(['http://facebook.com','http://twitter.com','http://google.com'])
#complexThread(['http://facebook.com','http://twitter.com','http://google.com'])
if __name__=="__main__":
    from random import sample
    with open('sample.txt','r',encoding='utf-8') as f:lis = [i.strip() for i in f.readlines()]
    lis =sample(lis,int(config.get('setting','count')))
    sleeptime = int(config.get('setting','sleeptime'))#5#[0,1,3,5,10][randint(0,4)]
    shortsleep = int(config.get('setting','shortsleep')) #* random()
    timeout = int(config.get('setting','timeout'))
    complexThread(lis,sleeptime=sleeptime,timeout=timeout)