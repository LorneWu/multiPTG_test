# -*- coding: utf-8 -*-
from ask import ask
from dec_time import dec_time
from configparser import ConfigParser;config = ConfigParser()
config.read('setting.ini')

@dec_time
def continuous(lis,sleeptime=0,timeout=0):
    #filename = 'testcontinuous_result'
    result = []
    for i in lis:result.append(ask(i,sleeptime,timeout))

        
    
if __name__=="__main__":
    from random import sample
    with open('sample.txt','r',encoding='utf-8') as f:lis = [i.strip() for i in f.readlines()]
    lis =sample(lis,int(config.get('setting','count')))
    sleeptime = int(config.get('setting','sleeptime'))#5#[0,1,3,5,10][randint(0,4)]
    shortsleep = int(config.get('setting','shortsleep')) #* random()
    timeout = int(config.get('setting','timeout'))
    continuous(lis,sleeptime=sleeptime,timeout=timeout)