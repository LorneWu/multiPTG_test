# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 14:49:35 2018

@author: lorne
"""
import requests
from bs4 import BeautifulSoup as bs
import re
import headers
import threading

class Thread (threading.Thread):
    def __init__(self,url,lock):
        threading.Thread.__init__(self)
        self.url = url
        self.sleeptime=0
        self.timeout=0
        self.lock = lock
    def run(self):
        try:
            r = requests.get(self.url,headers = headers.getprofile(),timeout=0.5)
            sp = bs(r.text,'html.parser')
            sp.title.text
        except:pass
        else:
            lock.acquire()
            with open('websites.txt','a',encoding='utf-8') as f:f.write(self.url+'\n')
            lock.release()
lock = threading.Lock()
with open('websites.txt','w',encoding='utf-8') as f:pass
for url in ['https://univ.cc/search.php?dom=edu&key=&start=','https://univ.cc/search.php?dom=world&key=&start=']:
    for i in range(1,7496,50):
        print(int(i/50))
        content = []
        r = requests.get(url+str(i))
        sp = bs(r.text,'html.parser')
        for i in sp.find_all('li'):
            content.append(i.find('a').get('href'))
    
        for j in content:
            thr = Thread(j,lock)
            thr.start()
        
    