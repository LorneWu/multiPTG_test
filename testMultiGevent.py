#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 18:16:23 2018

@author: lorne
"""
from dec_time import dec_time 
#from gevent import monkey; monkey.patch_all()
from multiprocessing import Pool
from ask2 import ask_grequests
from configparser import ConfigParser;config = ConfigParser()
config.read('setting.ini')


@dec_time
def multiGevent(lis,sleeptime=0,timeout=0):
    pcount = 10
    p = Pool(pcount)
    data = [lis[i:i+int(len(lis)/pcount)] for i in range(0,len(lis),int(len(lis)/pcount))]
    p.map(ask_grequests, data)
    
    
if __name__=="__main__":
    from random import sample
    with open('sample.txt','r',encoding='utf-8') as f:lis = [i.strip() for i in f.readlines()]
    lis =sample(lis,int(config.get('setting','count')))
    sleeptime = int(config.get('setting','sleeptime'))#5#[0,1,3,5,10][randint(0,4)]
    shortsleep = int(config.get('setting','shortsleep')) #* random()
    timeout = int(config.get('setting','timeout'))
    multiGevent(lis,sleeptime=sleeptime,timeout=timeout)