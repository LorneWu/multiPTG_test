# -*- coding: utf-8 -*-
import headers
import requests
from bs4 import BeautifulSoup as bs
import time
import logging

s = requests.Session()

def ask(url,sleeptime=0,timeout=0):
    try:
        if timeout==0:r = s.get(url,headers = headers.getprofile())
        else:r = s.get(url,headers = headers.getprofile(),timeout = timeout)
        #logging.warning(url + '\tresult :'+str(r.status_code))
    except Exception as e:
        #logging.warning(e)
        with open('wrong.log','a')as f:f.write(str(e)+'\n')
    time.sleep(sleeptime)
'''
def ask(url,sleeptime=0,timeout=0):
    
    if timeout==0:r = s.get(url,headers = headers.getprofile())
    else:r = s.get(url,headers = headers.getprofile(),timeout = timeout)
    logging.warning(url + '\tresult :'+str(r.status_code))
'''

