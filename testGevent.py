# -*- coding: utf-8 -*-
import gevent
#from ask import ask
from ask2 import ask_gevent,ask_grequests
from dec_time import dec_time
from gevent import pool
from configparser import ConfigParser;config = ConfigParser()
config.read('setting.ini')

@dec_time
def Gevent(lis,sleeptime=0,timeout=0):
    
    jobs = [gevent.spawn(ask_gevent,url,sleeptime,timeout) for url in lis]
    gevent.joinall(jobs)

@dec_time
def Gevent_grequests(lis,sleeptime=0,timeout=0):
    s = ask_grequests(lis,timeout)
    #print(s)

if __name__=="__main__":
    from random import sample
    with open('sample.txt','r',encoding='utf-8') as f:lis = [i.strip() for i in f.readlines()]
    lis =sample(lis,int(config.get('setting','count')))
    timeout = int(config.get('setting','timeout'))
    Gevent(lis,timeout=timeout)
    Gevent_grequests(lis,timeout=timeout)